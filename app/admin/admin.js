'use strict';

angular.module('myApp.adminIndex', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/admin', {
        templateUrl: 'admin/admin.html',
        controller: 'AdminIndexCtrl'
    });
}])

.controller('AdminIndexCtrl', ['$scope', function($scope) {

}]);