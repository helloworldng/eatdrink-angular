'use strict';

angular.module('myApp.home', ['ngRoute', 'ngCookies', 'angular-uuid', 'pubnub.angular.service'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'home/home.html',
    controller: 'HomeCtrl'
  });
}])

.controller('HomeCtrl', ['$scope', '$location', '$anchorScroll', '$rootScope', '$cookies', 'uuid', 'PubNub',
        function($scope, $location, $anchorScroll, $rootScope, $cookies, uuid, PubNub) {

	$scope.messages = [];

    PubNub.init({
        publish_key: 'pub-c-ecfc3624-b442-47ec-a36d-3f4d35d86e83',
        subscribe_key: 'sub-c-af12bf36-bd26-11e5-8a35-0619f8945a4f'
    });

    $scope.receiveMessage = function(msg){
        $scope.messages.push({ text: msg, type: "rcvd"});
        $scope.gotoBottom();
    }

    $scope.sendMessage = function(msg) {
        PubNub.ngPublish({
            channel  : "eatdrink",
            message  : msg,
            callback : function(info) { console.log(info) },
            triggerEvents: true
        });
    };

    $scope.subscribe = function() {

        PubNub.ngSubscribe({channel: $scope.uuid})

        $rootScope.$on(PubNub.ngMsgEv($scope.uuid), function (event, payload) {
            // payload contains message, channel, env...
            console.log('got a message event:', payload);
            $scope.$apply(function () {
                $scope.receiveMessage(payload.message);
            });
        });
    }

    $scope.addMessage = function(){
        if( $scope.currentUserMsg.trim() != ""){
            $scope.messages.push({ text: $scope.currentUserMsg, type: "sent"});
            $scope.gotoBottom();
            $scope.sendMessage($scope.currentUserMsg);
            $scope.currentUserMsg = "";
        }
    };

    $scope.gotoBottom = function() {
        // set the location.hash to the id of
        // the element you wish to scroll to.
        $location.hash('bottom');

        // call $anchorScroll()
        $anchorScroll();
    };

    if($cookies.get('channel')) {
        $scope.uuid = $cookies.get('channel');
    }else{
        $scope.uuid = uuid.v4();
        $cookies.put('channel', $scope.uuid)
    }


    $scope.sendMessage({ start: "true", channel: $scope.uuid}); // send initial PubNub message
    $scope.subscribe();
}])

.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});